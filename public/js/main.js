$(document).ready(function(){
    $(function() {
      $('a#scroll-down').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:$('#header').height()-77}, 'slow', 'linear');
      });
    });

	$('.pick-kvart span').click(function(){
		$('.kvart-btn').html(($(this).html()));

 		$('.list-group').empty();
 		getKvart($(this).data('id'));

	});

	function getKvart(id)
	{
		$.ajax({
		  url: "../../kvart/"+id,
		}).done(function(res) {

			$.each(res, function(key, kvart){

				$('.list-group').append(

	              '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start mb-3">'+
	                '<div class="d-flex w-100 justify-content-between mb-3">'+
	                  '<h5 class="mb-1">' + kvart.title + '</h5>'+
	                  '<small class="text-muted">' + kvart.dates + '</small>'+
	                '</div>'+
	                '<div class="d-flex w-100 justify-content-center align-items-center mb-3 flex-md-row flex-column">'+
	                    '<div class="col-12 col-md-4 p-0">'+
	                        '<img src="'+kvart.img+'" class="img-responsive" />'+
	                    '</div>'+
	                    '<div class="col-12 col-md-8">'+
	                         '<p class="mb-1">' + kvart.desc + '</p>'+
	                    '</div>'+
	                '</div>'+
	                '<small class="text-muted">Ne propustite priliku!</small>'+
	              '</a>'

				);

			});


		});

	}

});