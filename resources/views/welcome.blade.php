<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Climathon Rijeka | Green City Solutions © </title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/custom.css"> 
        <link rel="stylesheet" href="css/masonry.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/4a3b198e1b.js" crossorigin="anonymous"></script>


    </head>
    <body>

        <nav class="navbar navbar-expand-lg pt-3 pb-3 pl-5 pr-5 fixed-top navbar-light">
          <a class="navbar-brand pl-0 pl-md-5 text-white" href="#">
            Climathon Rijeka | Green City Solutions © 
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end pr-5" id="navbarNavAltMarkup">
            <div class="navbar-nav text-uppercase align-items-center text-center">
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Naslovna</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Odaberi<br>kvart</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Slikaj<br>otpad</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Osvoji<br>nagradu</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">Skini<br>aplikaciju</a>
              <a class="nav-item nav-link pr-3 pl-3 text-white" href="#">O<br>nama</a>
              <a class="nav-item nav-link pl-0 pl-md-5 text-white" href="#"><i class="fa fa-search"></i></a>
            </div>
          </div>

        </nav>

        <div class="d-flex align-items-center justify-content-center h-50 text-white" id="header">

            <div class="col-4"></div>
            <div class="col-12 col-md-4 p-2 text-center">
                <p class="text-uppercase ls txt-sm"></p>
                <h1 class="font-weight-light mb-4">Climathon Rijeka Challenge</h1>
                <p></p>
            </div>
            <div class="col-4"></div>

            <a href="#" class="hidden-xs hidden-sm" id="scroll-down">
                <span></span>
            </a>

        </div>


        <div class="d-flex bg-white p-5">


            <div class="col-2"></div>
            <div class="col-12 col-md-4 p-2">

            <h2 class="font-weight-normal mb-4 pb-2 mt-4">Odvoz glomaznog otpada, što s tim?</h2>
                <p class="mb-2 p-intro text-black-50">  
                    Naša platforma riješava problem glomaznog otpada u gradu: od odvoza do zbrinjavanja te pravilnog motiviranja građana. 
                </p>  
                    <h5 class="mt-3 mb-3">Kako prepoznati glomazni otpad?</h5>
                <p class="mb-2 p-intro text-black-50">            
                    Krupni (glomazni) otpad je predmet ili tvar koju je zbog volumena i /ili mase neprikladno prikupljati u sklopu usluge prikupljanja miješanog komunalnog otpada.
                    Zakon o održivom gospodarenju otpadom (NN 94/13, 73/17) zabranjuje odlaganje krupnog (glomaznog) otpada na javnim površinama.
                    <br>                     
                    Prema pravilniku Čistoća d.o.o., za ovu vrstu otpada možete, bez naknade, naručiti odvoz
                    jednom godišnje, odvoz do 2 m3.
                </p>

            </div>
            <div class="col-12 col-md-4 p-2">
                <h2 class="font-weight-normal mb-4 pb-2 mt-4">Nemate prijevoz i radnu snagu? Nudimo rješenje</h2>
                <p class="mb-2 p-intro text-black-50">           
                    Saznajte termine skupljanja otpada za Vaš kvart kako biste brzo i jednostavno riješili svoj problem sa otpadom. Odaberite svoju gradsku četvrt i organizirajte svoj raspored.
                </p>

            </div>
            <div class="col-2"></div>
        </div>

        <div class="d-flex align-items-center justify-content-center bg-white p-5 mb-5 flex-md-row flex-column">

            <div class="col-md-2 col-0"></div>
            <div class="col-12 col-md-4 p-2 text-center">

                <h5 class="mb-4">Odaberite kvart za koji želite saznati termin odlaganja</h5>

                <div class="dropdown">
                  <button class="btn btn-secondary dropdown-toggle kvart-btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Odaberite kvart
                  </button>
                  <div class="dropdown-menu pick-kvart" aria-labelledby="dropdownMenuButton">
                    <span class="dropdown-item" data-id="1" href="#">Kozala</span>
                    <span class="dropdown-item" data-id="2" href="#">Kantrida</span>
                    <span class="dropdown-item" data-id="3" href="#">Trsat</span>
                  </div>
                </div>

            </div>

            <div class="col-12 col-md-4 p-0 termini">

                <div class="list-group">
                </div>

            </div>


            <div class="col-md-2 col-0"></div>
        </div>


        <div class="d-flex justify-content-center bg-white p-5">

            <div class="col-2"></div>
            <div class="col-12 col-md-8 p-2 text-center">
                <h2 class="font-weight-normal mb-4 pb-2 mt-4">Uočili ste nered na Vašoj ulici?<br> Pošalji sliku, pomogni očuvanju čistoće grada i osvojite nagradu</h2>
                <div class="col-12 text-center">
                    <img style="margin: 0 auto" src="/img/qr.jpg" class="img-responsive">
                </div>


                <div class="d-flex col-12 flex-md-row flex-column mb-5 mt-5">
                        <div class="col-12 col-md-6 text-center mb-5">
                            <button type="button" class="btn btn-secondary btn-lg">Učitaj sliku otpada <i class="fas fa-camera"></i></button>
                        </div>
                        <div class="col-12 col-md-6 text-center mb-5">
                            <button type="button" class="btn btn-secondary btn-lg">Pošalji</button>
                        </div>
                </div>

                <div>
                </div>

                <div class="d-flex align-items-center justify-content-center bg-white p-5 flex-md-row flex-column mb-5 mt-5">

                    <div class="col-2"></div>
                    <div class="col-12 col-md-8 p-0">
                        <form>
                          <div class="form-row">

                              <div class="form-group col-12">
                                <input type="text" class="form-control" id="inputAddress" placeholder="Adresa / Ulica">
                              </div>
                              <div class="form-group col-12">
                                <input type="text" class="form-control" id="inputAddress2" placeholder="Grad">
                              </div>
                            <div class="col-12 mt-3 text-center">
                                <p>ili automatski učitaj lokaciju otpada</p>                                
                            </div>

                            <div class="col-12 mb-5 mt-3 text-center">
                                <i class="fas fa-map-marker-alt"></i>                     
                            </div>

                          </div>

                          <div class="form-row">

                            <div class="form-group col-md-6">
                              <input type="text" class="form-control" id="inputEmail4" placeholder="Ime">
                            </div>
                            <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="inputAddress" placeholder="Prezime">
                            </div>

                            <div class="col-12 mt-3 text-center">
                                <p>ili se ulogirajte putem društvenih mreža</p>                                
                            </div>

                            <div class="col-12 mb-5 mt-3 text-center">
                                <i class="fab fa-facebook"></i>   
                                <i class="fab fa-linkedin"></i>                       
                            </div>

                            

                            <div class="form-group col-12">
                            <input type="text" class="form-control" id="kontaktTelefon" placeholder="Kontakt telefon">
                            </div>
                          </div>
                          <div class="form-group">
                          <button type="submit" class="btn btn-secondary">Pošalji</button>
                          </div>
                        </form>
                    </div>
                    <div class="col-2"></div>
                    
                </div>


                <div class="col-12 p-2 p-md-5 text-center mb-5">
                    <h2 class="font-weight-normal mb-4 pb-2 mt-4">Osvoji nagradu</h2>
                    <p class="mb-2 p-intro text-black-50">                    
                        Zahvaljujući našim strateškim partnerima u Rijeci i okolici, Čistoća d.o.o. pokrenuo je program "Ne budi smeće" kako bi potakli građane i goste Rijeke na održivi model izgradnje čišće okolice. Time će svi građani i gosti grada koji teče pomoći u usvajanju Agende 2030 i Ciljeva održivog razvoja UN-a te lansirati grad Rijeku među gradove lidere za ekološku osviještenost u Europskoj Uniji i šire. U Green City Solutions posvećeni smo zaštiti okoliša i ekološkog zbrinjavanja otpada doprinoseći ulogu Rijeke u sklopu međunarodnog saveza gradova "The Milan Charter" i "Convenant of Mayors for Climate and Energy". 
                    </p>
                    <p class="mb-2 p-intro text-black-50">                    
                        Nagradu naših lokalnih partnera možete osvojiti putem prijave ilegalnog odlaganja otpada i pravilnog razvrstavanja hrane i drugih organskih ostataka. Nagrade uključuju:
                    </p>
                    <p class="p-intro text-black-50">- dnevni parking u Rijeci</p>
                    <p class="p-intro text-black-50">- naušnice i ostali nakit</p>
                    <p class="p-intro text-black-50">- ulaznice na koncerte, za kino i kazališne predstave</p>
                    <p class="p-intro text-black-50">- jednodnevno putovanje</p>
                    <p class="p-intro text-black-50">- zemlja za Vaš vrt</p>

                </div>


                <p class="mb-4 p-intro text-black-50">              
                    Skini aplikaciju E-otpadnici na svoj mobilni uređaj - <a href="https://play.google.com/store/apps/details?id=hr.cistoca_ri.javnost&hl=en" target="_blank">web</a>
                </p>
                <div class="col-12 text-center">
                    <img style="margin: 0 auto" src="/img/e-otpadnici.png" class="img-responsive">
                </div>

            </div>
            <div class="col-2"></div>
        </div>

        <div class="col-12 mt-3 mb-3 text-center p-5">
            <h3 class="mb-5">O nama</h3>

            <p class="mb-5 p-intro text-black-50">              
                Green City Solutions je dinamičan tim od 7 eko-entuzijasta iz Rijeke i okolice. Želja nam je da ozelenimo naš grad novom tehnološkom solucijom. Pridružite nam se! 
                <br>
                <i>
                - Alice, Ana, Gabriella, Ivana, Matija, Neven, Vedran
                </i>

            </p>

            <img style="margin: 0 auto" src="/img/mi.jpeg" class="img-responsive">
        </div>



        <div class="d-flex align-items-center justify-content-center bg-white p-5 mb-5">

        </div>



        <!--

        <div class="d-flex align-items-center flex-md-row flex-column justify-content-center why-div">

            <div class="col-12 col-md-6 left-div align-self-stretch">
                
            </div>
            <div class="col-12 col-md-6 right-div p-5">
                <div class="col-12 col-md-10 p-3 text-center text-md-left">
                    <h2 class="font-weight-normal mb-5 mt-5">Why choose Feeder</h2>
                    <p class="mb-4 p-intro text-black-50">                    
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu aliquam ipsum. Nulla vehicula erat porta, rhoncus dolor quis, facilisis ipsum. Aenean ullamcorper, odio in tempus suscipit, mi sem feugiat lectus, ac pharetra libero quam non metus. Etiam hendrerit sit amet metus eu feugiat. Vestibulum lobortis, sem eget varius vulputate, dui magna varius mi, vel ultricies arcu ligula ac ligula. Cras non quam id sapien vehicula commodo. Ut ultricies justo et commodo aliquam. Integer congue aliquam sem. Nulla vulputate enim ac pulvinar pharetra. Nullam non dui ornare, pulvinar mauris et, luctus quam. Nulla eu vulputate mi, at suscipit nunc.
                    </p>
                </div>
            </div>
        </div>

        <div class="d-flex align-items-center justify-content-center bg-white pt-5 pb-2">

            <div class="col-2"></div>
            <div class="col-12 col-md-8 p-0 text-center">
                <h2 class="font-weight-normal mb-4 pb-2 mt-4">Portfolio</h2>
                <p class="p-intro text-black-50 mb-4">                    
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>
            </div>
            <div class="col-2"></div>

        </div>

        
        <div class="button-group filters-button-group mb-5 pb-3 pt-3">
            <button id="all-masonry" class="mb-3 ls text-uppercase font-weight-bolder button is-checkedpl-0 pr-0 ml-4 mr-4" data-filter="*">All</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".design">Design</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".identity">Identity</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".photography">Photography</button>
            <button class="mb-3 ls text-uppercase font-weight-bolder button pl-0 pr-0 ml-4 mr-4" data-filter=".web">Web design</button>
        </div>

        <section id="grid-container" class="transitions-enabled fluid masonry js-masonry grid">
            <article class="design">
                <img src="img/design1.jpg" class="img-responsive" />
            </article>
            <article class="design">
                <img src="img/design2.jpg" class="img-responsive" />
            </article>
            <article class="design">
                <img src="img/design3.jpg" class="img-responsive" />
            </article>
            <article class="identity">
                <img src="img/identity.png" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo1.jpg" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo2.jpg" class="img-responsive" />
            </article>
            <article class="photography">
                <img src="img/photo3.jpg" class="img-responsive" />
            </article>
            <article class="web">
                <img src="img/web1.jpg" class="img-responsive" />
            </article>
            <article class="web">
                <img src="img/web2.jpg" class="img-responsive" />
            </article>
        </section>

        -->


    <script src="js/main.js" type="text/javascript" ></script>
    <script src="js/masonry.js" type="text/javascript" ></script>
    </body>
</html>
