<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Kvart as KvartResource;
use App\Kvart;

class KvartController extends Controller
{
    public function json($id)
    {    	
	    $kvarts = [];

	    switch ($id) {
		    case 1:
		    //kozala
		    	$k1 = new Kvart(
		    		'ul. Tizianova 1',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Ponedjeljak 11.11.2019.'
		    		);


		    	$k2 = new Kvart(
		    		'ul. Laginjina 23',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Utorak 19.11.2019.'
		    		);
		    	$kvarts[] = new KvartResource($k1);
		    	$kvarts[] = new KvartResource($k2);
		        break;
		    case 2:
		    //kantrida
		    	$k1 = new Kvart(
		    		'ul. Milutina Baraća 60',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Srijeda 13.11.2019.'
		    		);
		    	$k2 = new Kvart(
		    		'ul. Kantrida 19',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Petak 22.11.2019.'
		    		);
		    	$k3 = new Kvart(
		    		'ul. Baričićeva 12',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Četvrtak 28.11.2019.'
		    		);
		    	$kvarts[] = new KvartResource($k1);
		    	$kvarts[] = new KvartResource($k2);
		    	$kvarts[] = new KvartResource($k3);
		        break;
		    case 3:
		    //trsat
		    	$k1 = new Kvart(
		    		'ul. Trsat 1',
		    		'Kontejner će biti na Vašem raspolaganju 24 sata.',
		    		'Četvrtak 14.11.2019.'
		    		);
		    	$kvarts[] = new KvartResource($k1);
		        break;
		}

    	//die(var_dump($id));
	    return response()->json($kvarts);
    }
}
