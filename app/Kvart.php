<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kvart extends Model
{
    public $title;
    public $img = '/img/rijeka.jpg';
    public $desc;
    public $dates;

    public function __construct($title, $desc, $dates)
    {
    	$this->title = $title;
    	$this->desc = $desc;
    	$this->dates = $dates;    	
    }

    /*

    public function getSurface($radius)
    {
    	//pi - 3.1415926535898
    	$surface = pi() * $radius * $radius;

    	return round($surface, 2);
    }

    public function getCircumference($radius)
    {
    	//pi - 3.1415926535898
    	$circumference = 2 * $radius * pi();
    	
    	return round($circumference, 2);
    }
    */

}
